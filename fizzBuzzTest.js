describe('fizzBuzz',()=>{
    it('returns Fizz if the number is divisible by three',()=>{

        const numberDivisibleByThree = 3

        const result = fizzBuzz(numberDivisibleByThree)

        expect(result).toBe('Fizz')

    })

    it('returns Buzz if the number is divisible by five', ()=>{

        const numberDivisibleByFive = 5

        const result = fizzBuzz(numberDivisibleByFive)

        expect(result).toBe('Buzz')

    })

    it('returns FizzBuzz if the number is divisible by three and five', ()=>{

        const numberDivisibleByThreeAndFive = 15

        const result = fizzBuzz(numberDivisibleByThreeAndFive)

        expect(result).toBe('FizzBuzz')
    })

    it('returns the number not divisible neither by 3 nor 5', ()=>{
        
    })


})

function fizzBuzz(number){

    if(isDivisibleByThree(number) && isDivisibleByFive(number)){
        return 'FizzBuzz'
    } else if(isDivisibleByFive(number)){
        return 'Buzz'
    }else if (isDivisibleByThree(number)){
        return 'Fizz'
    } 
    return ''+ number
    
}

function isDivisibleByThree(number){
    return (number % 3 === 0)
}

function isDivisibleByFive(number){
    return (number % 5 === 0)
} 
